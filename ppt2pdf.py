import comtypes.client
import os
import sys
import shutil

def pps_to_ppt(ppsFileName):
    if ppsFileName[-4:] == '.pps':
        pptFileName = ppsFileName[:-3] + 'ppt'
        os.rename(ppsFileName,pptFileName)
        return pptFileName
    else:
        return ppsFileName

def ppt_to_pdf(powerpoint, pptFileName, pdfFileName, formatType = 32):
    deck = powerpoint.Presentations.Open(pptFileName)
    deck.SaveAs(pdfFileName, formatType) # formatType = 32 for ppt to pdf
    deck.Close()

"初始化PowerPoint,Windows必须安装有Powerpoint"
def init_powerpoint():
    powerpoint = comtypes.client.CreateObject("Powerpoint.Application")
    powerpoint.Visible = 1
    return powerpoint

def handle_pps_file(folder):
    fileList=[]
    for root,dirs,files in os.walk(folder):
        for file in files:
            fullPath = root + "\\"
            fullPath = fullPath + file
            if fullPath[-4:]=='.pps':
                fullPath = pps_to_ppt(fullPath)
                fileList.append(fullPath)
            else:
                fileList.append(fullPath)
    return fileList
 
if __name__ == "__main__":
    powerpoint = init_powerpoint()
    cwd = os.getcwd()
    if len(sys.argv)==2:
        cwd += '\\'
        cwd += sys.argv[1]
    else:
        print('usage: python ppt2pdf.py ppt-dir')
        sys.exit()
    if not os.path.exists(cwd):
        print('not exist:',cwd,'!!!')
        sys.exit()
    fileList = handle_pps_file(cwd)
    # convert_files_in_folder(powerpoint, cwd)
    outdir = os.path.dirname(cwd) + '\\output'
    print('outdir =',outdir)
    for file in fileList:
        outfile = file[len(cwd):]
        outfile = outdir + outfile
        copyFile = False
        if outfile[-4:]=='.ppt' :
            outfile = outfile[:-4] + '.pdf' 
        elif outfile[-5:]=='.pptx':
            outfile = outfile[:-5] + '.pdf'
        else:
            copyFile = True
        pdfFileDir = os.path.dirname(outfile)
        if (not os.path.exists(pdfFileDir)):
            os.makedirs(pdfFileDir)
        if copyFile:
            shutil.copy(file,outfile)
        else:
            print('开始转换:',file)
            ppt_to_pdf(powerpoint,file,outfile)
            print('转换完成:',outfile)

    powerpoint.Quit()
